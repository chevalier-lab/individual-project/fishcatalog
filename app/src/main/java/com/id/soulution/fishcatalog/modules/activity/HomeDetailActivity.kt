package com.id.soulution.fishcatalog.modules.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.id.soulution.fishcatalog.R
import com.id.soulution.fishcatalog.helpers.DateTimeHelper
import com.id.soulution.fishcatalog.modules.fragments.*
import com.id.soulution.fishcatalog.modules.models.User
import java.lang.StringBuilder

class HomeDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_detail)
        if (supportActionBar != null)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)

        this.init()
        this.bind()
        this.actions()
    }

    private fun init() {}

    private fun bind() {}

    private fun actions() {
        // Set Fragment
        val selection: Int = intent.getIntExtra("selection", 0)
        this.setFragment(selection)
        when (selection) {
            0 -> setTitle(R.string.label_type)
            1 -> setTitle(R.string.label_location)
            2 -> setTitle(R.string.label_category)
            else -> setTitle(R.string.label_about_us)
        }
    }

    // Set Fragment
    private fun setFragment(position: Int) {
        val fragment: Fragment = when (position) {
            0 -> TypeFragment.newInstance()
            1 -> LocationFragment.newInstance()
            2 -> CategoryFragment.newInstance()
            else -> AboutUsFragment.newInstance()
        }
        supportFragmentManager.beginTransaction()
            .replace(R.id.home_detail_frame, fragment)
            .commit()
    }
}
