package com.id.soulution.fishcatalog.modules.items

data class UserItem (
    var uri: String = "",
    var label: String = ""
)