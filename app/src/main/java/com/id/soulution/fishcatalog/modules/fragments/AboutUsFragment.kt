package com.id.soulution.fishcatalog.modules.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.id.soulution.fishcatalog.R
import com.id.soulution.fishcatalog.modules.adapters.AccountMenuAdapter
import com.id.soulution.fishcatalog.modules.adapters.UserAdapter
import com.id.soulution.fishcatalog.modules.items.AccountMenuItem
import com.id.soulution.fishcatalog.modules.items.UserItem
import java.lang.StringBuilder

class AboutUsFragment: Fragment() {

    companion object {
        fun newInstance(): AboutUsFragment {
            return AboutUsFragment()
        }
    }

    private lateinit var mainList: RecyclerView
    private lateinit var mainAppsIdentityUser: TextView
    private lateinit var mainAppsIdentityDate: TextView

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about_us, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Bind
        this.mainList = view.findViewById(R.id.home_detail_list)
        this.mainAppsIdentityUser = view.findViewById(R.id.main_apps_identity_user)
        this.mainAppsIdentityDate = view.findViewById(R.id.main_apps_identity_date)
        val adapter = UserAdapter() { _, _ ->

        }
        val userItems: MutableList<UserItem> = arrayListOf()
        userItems.add(UserItem("https://scontent-sin6-1.xx.fbcdn.net/v/t1.15752-9/92043946_671481340329972_3971105394419302400_n.jpg?_nc_cat=101&_nc_sid=b96e70&_nc_eui2=AeF9_gnwpnxjPJndAvqPWV3W-XuBLDGlQeP5e4EsMaVB46WU_7PSuqRWpnlbCDsKtFe3Rknf3eBOjyPKr-gZFOVz&_nc_ohc=qrQ5-klZbgoAX-ZTZX1&_nc_ht=scontent-sin6-1.xx&oh=4c7929a3b8c240e5146ba90d70bce10b&oe=5EAEB824", "Rahmat Mulya Simanjuntak"))
        userItems.add(UserItem("https://scontent-sin6-2.xx.fbcdn.net/v/t1.15752-9/92360465_154182439236305_74717763637084160_n.jpg?_nc_cat=102&_nc_sid=b96e70&_nc_eui2=AeEKcW0ZRm1dgJamH8_f-xtKwcDvxHrTJQHBwO_EetMlASh7HToRr2DXuDdaCUZXo9CW1Z-jBeZffRrLX7vjG9Ws&_nc_ohc=_qjAtXqL6RUAX_lRrpg&_nc_ht=scontent-sin6-2.xx&oh=258eb54351aa86f2d267906ed0c860e2&oe=5EAE5CEF", "Andreas Pakpahan"))

        this.mainAppsIdentityUser.text = StringBuilder().append("Fish Catalogue merupakan aplikasi yang berisi informasi mengenai ikan mulai dari jenis, spesies, dan lainnya")
        this.mainAppsIdentityDate.text = StringBuilder().append("Fish Catalogue")

        // Action
        this.mainList.setBackgroundColor(0)
        adapter.items = userItems
        this.mainList.layoutManager = GridLayoutManager(context, 2)
        this.mainList.adapter = adapter
    }

}